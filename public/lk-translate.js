LarakitJs.initSelector('.js-change-locale-admin', function () {
    $(this).on('click', function () {
        var locale = $(this).attr('data-locale');
        $.post('/!/ajax/change-locale-admin', {locale: locale}, function (response) {
            if ('success' == response.result) {
                location.href = location.href;
            }
        });
        return false;
    });
});

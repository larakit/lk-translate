<?php
namespace Larakit\Controllers;

use BootstrapDialog\BootstrapDialog;
use Larakit\Attaches\AttachControllerInterface;
use Larakit\Attaches\TraitControllerAttaches;
use Larakit\Controller as LarakitController;
use Larakit\CRUD\TraitControllerCRUD;
use Larakit\CRUD\TraitEntity;
use Larakit\Thumb\ThumbControllerInterface;
use Larakit\Thumb\TraitControllerThumb;
use Larakit\Thumb\TraitControllerThumbs;

class AdminLarakitTranslateController extends LarakitController implements AttachControllerInterface, ThumbControllerInterface {

    use TraitEntity;
    use TraitControllerCRUD;
    use TraitControllerAttaches;
    use TraitControllerThumb;
    use TraitControllerThumbs;

    static function getEntityPrefix() {
        return 'Admin';
    }

    static function getEntitySuffix() {
        return 'Controller';
    }

    function routeBase() {
        return ROUTE_ADMIN_LARAKIT_TRANSLATE;
    }

    function crudWrapFormDialog(BootstrapDialog $dialog) {
        return $dialog->setSizeWide();
    }

    function attachTypes() {
        return [
            'default',
        ];
    }

    function thumbsTypes() {
        return [
            'illustration'
        ];
    }


    function thumbTypes() {
        return [
            'default',
        ];
    }


}
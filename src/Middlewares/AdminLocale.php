<?php

namespace Larakit\Middlewares;

use Closure;
use Larakit\Translate\LangManager;

class AdminLocale {
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next) {
        \App::setLocale(LangManager::getCurrent());
        
        return $next($request);
    }
}

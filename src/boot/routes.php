<?php
\Larakit\Route\Route::ajax('change-locale-admin')
    ->setUses(function () {
        $locale = Request::get('locale');
        \Larakit\Translate\LangManager::setCurrent($locale);
        
        return [
            'result' => 'success',
        ];
    })
    ->addMiddleware('web')
    ->put('post');
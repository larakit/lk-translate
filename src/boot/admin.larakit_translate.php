<?php
//################################################################################
//  роуты
//################################################################################
define('ROUTE_ADMIN_LARAKIT_TRANSLATE', 'admin.larakit_translate');
$r = \Larakit\CRUD\Route::crud(ROUTE_ADMIN_LARAKIT_TRANSLATE, 'admincp/larakit-translates', 'fa fa-folder-open',['web','auth','admin'],'\Larakit\Controllers');

//################################################################################
//  меню
//################################################################################
\Larakit\Widgets\WidgetSidebarMenu::group('Локализация')
    ->addItem('larakit_translate', 'Перевод интерфейса', ROUTE_ADMIN_LARAKIT_TRANSLATE);

//################################################################################
//  SEO
//################################################################################
\Larakit\SEO::title(ROUTE_ADMIN_LARAKIT_TRANSLATE, 'Перевод интерфейса');
<?php
namespace App\Validators;

use Larakit\ValidateBuilder;

class LarakitTranslateValidator extends ValidateBuilder {

    function build() {
        $this
            //############################################################
            //составляем правила для поля "agree"
            //############################################################
            ->to('agree')
            ->ruleRequired('Для участия на проекте необходимо принять его правила');
    }

}
<?php
namespace App\FormFilters;

use Larakit\FormFilters\FilterLike;
use Larakit\FormFilters\FormFilter;

class LarakitTranslateFormFilter extends FormFilter {

    function init() {
        $this->addFilter(
            FilterLike::factory('name')
                ->label('Название')
        );
    }
}
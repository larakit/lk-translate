<?php

namespace Larakit\Commands;

use Illuminate\Console\Command;
use Larakit\Helpers\HelperText;
use Larakit\Models\LarakitTranslate;

class TranslateImport extends Command {
    
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'larakit:translate-import';
    
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Импортируем данные из файлов переводов в БД';
    
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }
    
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        $dir        = resource_path('lang/' . config('app.locale'));
        $files      = rglob('*.php', 0, $dir);
        $translates = [];
        foreach($files as $file) {
            $key              = str_replace($dir . '/', '', $file);
            $key              = mb_substr($key, 0, -4);
            $content          = include $file;
            $translates[$key] = \Illuminate\Support\Arr::dot($content);
        }
        $locales = array_keys((array) config('app.locales'));
        $cnt_all = LarakitTranslate::count();
        foreach(LarakitTranslate::all() as $model) {
            $model->delete();
        }
        $cnt_import = 0;
        $cnt_updated = 0;
        $cnt_created = 0;
        //        dd($locales, $translates);
        foreach($translates as $file => $phrases) {
            foreach($phrases as $code => $content) {
                foreach($locales as $locale) {
                    $cnt_import++;
                    $model = LarakitTranslate::withTrashed()->firstOrCreate([
                        'code' => $code,
                        'file' => $file,
                        'lang' => $locale,
                    ]);
                    if($model->created_at == $model->updated_at) {
                        $cnt_created++;
                    } else {
                        $cnt_updated++;
                    }
                    $model->restore();
                    $cnt_updated++;
                    //                    dd($content);
                    if(!$model->content) {
                        $model->content = $content;
                        $model->save();
                    }
                }
            }
        }
        $this->info(HelperText::plural_with_number($cnt_created, 'запись добавлена', 'записи добавлены', 'записей добавлены', 'Добавленных записей нет'));
        $this->info(HelperText::plural_with_number($cnt_updated, 'запись обновлена', 'записи обновлено', 'записей обновлено', 'Обновленных записей нет'));
        $this->info(HelperText::plural_with_number($cnt_import, 'запись импортирована', 'записи импортировано', 'записей импортировано', 'Импортированных записей нет'));
        $this->info(HelperText::plural_with_number(count($translates), 'файл обработан', 'файла обработано', 'файлов обработано'));
    }
}

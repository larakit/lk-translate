<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateLarakitTranslatesTable extends Migration {
    
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('larakit__translates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('lang');
            $table->string('file');
            $table->string('code');
            $table->text('content');
            $table->timestamps();
            $table->softDeletes();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::drop('larakit__translates');
    }
}

<?php
namespace App\QuickForms;

use Larakit\QuickForm\ILaraForm;
use Larakit\QuickForm\LaraForm;

class LarakitTranslateForm extends LaraForm implements ILaraForm{

    /**
     * @return LaraForm
     */
    static function factory() {
        return new LarakitTranslateForm('larakit_translate');
    }

    function build() {
        $this
            ->putTextTwbs('name')
            ->setLabel('Название группы')
            ->setDesc('Название группы должно быть уникальным');
        $this
            ->putNumberTwbs('order')
            ->setLabel('Приоритет');
    }

    function getValidatorClass() {
        return \App\Validators\LarakitTranslateValidator::class;
    }


}
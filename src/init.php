<?php
\Larakit\Boot::register_migrations(__DIR__ . '/migrations');
\Larakit\Boot::register_boot(__DIR__ . '/boot');
\Larakit\Boot::register_command(\Larakit\Commands\TranslateImport::class);
\Larakit\Boot::register_view_path(__DIR__ . '/views', 'lk-translate');
\Larakit\Widgets\WidgetNavbarMenu::addItem('lk-translate::navbar_translate');

\Larakit\Twig::register_function('lk_translate_current_code', function () {
    return \Larakit\Translate\LangManager::getCurrent();
});

\Larakit\Twig::register_function('lk_translate_current_name', function () {
    $locales = config('app.locales');
    $code    = \Larakit\Translate\LangManager::getCurrent();
    
    return '[' . \Illuminate\Support\Str::upper($code) . '] ' . $locales[$code];
});

\Larakit\StaticFiles\Manager::package('larakit/lk-translate')
    ->jsPackage('lk-translate.js')
    ->setSourceDir('public');

\Larakit\Boot::register_middleware_group('admin', \Larakit\Middlewares\AdminLocale::class, 1);
<?php
namespace Larakit\Translate;

use Illuminate\Support\Str;

class LangManager {
    
    /**
     * Получить дефолтную кодировку браузера
     */
    static function defaultBrowserLocale() {
        $locales = config('app.locales');
        if(!count($locales)) {
            $locales[config('app.locale')] = Str::upper(config('app.locale'));
        }
        if(($list = strtolower($_SERVER['HTTP_ACCEPT_LANGUAGE']))) {
            if(preg_match_all('/([a-z]{1,8}(?:-[a-z]{1,8})?)(?:;q=([0-9.]+))?/', $list, $list)) {
                $language = array_combine($list[1], $list[2]);
                foreach($language as $n => $v) {
                    $language[$n] = $v ? $v : 1;
                }
                arsort($language, SORT_NUMERIC);
                $language = array_keys($language);
            }
        } else {
            $language = [];
        }
        foreach($language as $lang) {
            if(isset($locales[$lang])) {
                return $lang;
            }
        }
        
        return config('app.locale');
    }
    
    static function getCurrent() {
//        dd(\Session::get('admin_locale'));
        return \Session::get('admin_locale', config('app.locale'));
    }
    
    static function setCurrent($v) {
        $locales = config('app.locales');
        $v       = isset($locales[$v]) ? $v : config('app.locale');
        return \Session::put('admin_locale', $v);
    }
    
}
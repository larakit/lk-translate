<?php
namespace Larakit\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Larakit\CRUD\TraitEntity;
use Larakit\Models\LarakitModel;

/**
 * @mixin \Eloquent
 */
class LarakitTranslate extends LarakitModel {
    
    use TraitEntity;
    use SoftDeletes;
    
    protected $default_sort = [
        'id' => 'desc',
    ];
    
    protected $table    = 'larakit__translates';
    protected $perPage  = 100;
    protected $fillable = [
        'code',
        'file',
        'content',
        'lang',
    ];
    protected $hidden   = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    
    function __toString() {
        return $this->key;
    }
    
    /**
     * @return string
     */
    static function messageNotFound() {
        return 'Запись не найдена!';
    }
    
}

LarakitTranslate::creating(function ($model) {
    if(!$model->code || !$model->file || !$model->lang) {
        return false;
    }
    
    return true;
});
LarakitTranslate::updating(function ($model) {
    if(!$model->code || !$model->file || !$model->lang) {
        $model->delete();
        
        return false;
    }
    
    return true;
});